import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import HomeMoutons from "../pages/Mouton/HomeMoutons";
import HomeEleveur from "../pages/Eleveur/HomeEleveur";
import AddMouton from "../pages/Mouton/AddMouton";

import DetailsCommande from "../pages/Commande/DetailsCommande";
import ValidationDetailsCommande from "../pages/Commande/DetailsCommande_validation";

import AddEleveur from "../pages/Eleveur/AddEleveur";
import DetailsEleveur from "../pages/Eleveur/DetailsEleveur";
import UpdateEleveur from "../pages/Eleveur/UpdateEleveur";
import UpdateMouton from "../pages/Mouton/UpdateMouton";

import HomeCommandePrisEnCharge from "../pages/Commande/HomeCommandePrisEnCharge";
import HomeCommandesHistorique from "../pages/Commande/HomeCommandesHistorique";
import HomeCommandeValidation from "../pages/Commande/HomeCommandeValidation";
import DetailsMouton from "../pages/Mouton/DetailsMouton"
import Login from "../pages/Login";
import SignUp from "../pages/SignUp";
// import Techniciens from "../pages/admin/HomeUsers"
import Header from "../pages/Header";
import Footer from "../pages/Footer";
import MoutonsEleveur from "../pages/Eleveur/HomeSheepsParEleveur"
class Container extends Component {
  render() {
    return (
      <div>
        <Header />
        <BrowserRouter>
        {/* <Route exact path="/Techniciens" component={Techniciens} /> */}
          <Route exact path="/MoutonsEleveur" component={MoutonsEleveur} />
          <Route exact path="/Annonces" component={HomeMoutons} />
          <Route exact path="/Eleveurs" component={HomeEleveur} />
          <Route exact path="/AddAnnonce" component={AddMouton} />
          <Route exact path="/DetailsCommande" component={DetailsCommande} />
          <Route exact path="/ValidationDetailsCommande" component={ValidationDetailsCommande} />
          
          <Route exact path="/AddEleveur" component={AddEleveur} />
          <Route exact path="/DetailsEleveur" component={DetailsEleveur} />
          <Route exact path="/UpdateEleveur" component={UpdateEleveur} />
          <Route exact path="/UpdateAnnonce" component={UpdateMouton} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/" component={Login} />
          <Route exact path="/register" component={SignUp} />
          <Route exact path="/DetailsMouton" component={DetailsMouton} />

          <Route exact path="/HomeCommandePrisEnCharge" component={HomeCommandePrisEnCharge} />
          <Route exact path="/HomeCommandesHistorique" component={HomeCommandesHistorique} />
          <Route exact path="/HomeCommandeValidation" component={HomeCommandeValidation} />

          {/* DetailsCommande */}
        </BrowserRouter>
        <Footer />
      </div>
    );
  }
}

export default Container;
