import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";

import Techniciens from "../pages/admin/HomeUsers";
import Header from "../pages/admin/Header";

class ContainerAdmin extends Component {
  render() {
    return (
      <div>
        {" "}
        <Header />
        <BrowserRouter>
          <Route exact path="/Techniciens" component={Techniciens} />

          {/* DetailsCommande */}
        </BrowserRouter>
      </div>
    );
  }
}

export default ContainerAdmin;
