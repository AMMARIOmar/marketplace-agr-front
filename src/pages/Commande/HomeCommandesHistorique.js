import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
class HomeCommandesHistorique extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      Commandes: [],
      CmdSearch: [],
      redirect: false,

      // mouton: {},
      // showAvance: false,
      // showReste: false,
    };
    this.onChange = this.onChange.bind(this);
    // this.HandlePrEnCharge = this.HandlePrEnCharge.bind(this);
  }
  // elv = (id) => {
  //   axios
  //     .get("http://127.0.0.1:8000/api/mouton/" + id)
  //     .then((res) => {
  //       //  console.log(res.data.objet)
  //       this.setState({ mouton: res.data.objet });
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };

  componentDidMount() {
    // const token = localStorage.getItem("usertoken");
    // if (!token) {
    //   this.props.history.push("/login");
    // } else {
    // console.log(token);
    const idt = localStorage.getItem("usertoken");
    axios
      .get("http://127.0.0.1:8000/api/commandes/" + idt, {
        headers: {
          // "x-access-token": token, // the token is a variable which holds the token
          "Content-Type": "application/json",
        },
        // params: {
        //   statut: "en attente de validation reçu",
        //   order_by: "date_creation",
        //   order_mode: "asc",
        //   // idTechnicen:""
        //   // // idElv in [Techienc->elevs.id_el]
        // },
      })

      .then((res) => {
        // console.log(res.data);
        this.setState({
          Commandes: res.data,
          CmdSearch: res.data,
        });
      });
  }

  onChange(e) {
    const n = e.target.name,
      v = e.target.value;

    if (v.length == 8) {
      const cmd = this.state.Commandes.filter(
        (Commandes) => Commandes.mouton.boucle === v
      );
      this.setState({
        Commandes: cmd,
      });
    } else {
      this.setState({
        Commandes: this.state.CmdSearch,
      });
    }
  }

  render() {
    const cmd = this.state.Commandes.filter(
      (Commandes) =>
        Commandes.statut == "en attente de paiement" ||
        Commandes.statut == "reçu reste refusé" ||
        Commandes.statut == "reçu avance refusé" ||
        Commandes.statut == "livré"
    );

    let titre;
    if (cmd.length == 1 || cmd.length == 0) {
      titre = (
        <h6>
          <span>{cmd.length}</span> Commande{" "}
        </h6>
      );
    } else {
      titre = (
        <h6>
          <span>{cmd.length}</span> Commandes{" "}
        </h6>
      );
    }
    return (
      <div>
        <center>
          <nav class="header__menu filter__item">
            <ul>
              <li>
                <a href="/HomeCommandeValidation"> Commandes à valider</a>
              </li>
              <li>
                <a href="/HomeCommandePrisEnCharge">
                  {" "}
                  Commandes à prendre en charge
                </a>
              </li>
              <li className="active">
                <a href="/HomeCommandesHistorique"> Historique des commandes</a>
              </li>
            </ul>
          </nav>
        </center>

        {/* <div className="col-lg-8 col-md-8"> 
              <div className="hero__search text-right">
                  {" "}
                  <div className="hero__search__form text-right">
                    <form>
                      <span>Numéro de boucle</span>{" "}
                      <input type="text" onChange={this.onChange} />
                    </form>
                  </div>
                </div>
        </div> */}

        <section className="">
          <div className="container">
            {/* <h4 class="latest-product__item">Mes commandes</h4> */}
            <div className="row">
              <div className="col-lg-12 col-md-7">
                {/*<!-- Sheeps Grid Section Begin --> */}

                <div className="col-lg-8 col-md-8">
                  <div className="hero__search ">
                    {" "}
                    <center>
                      {" "}
                      <div className="hero__search__form">
                        <form>
                          <span>Numéro de boucle</span>{" "}
                          <input type="text" onChange={this.onChange} />
                          <i class="fa fa-search"></i>
                        </form>
                      </div>
                    </center>
                  </div>
                  <div className="filter__found text-left">
                    <h6>
                      <span>{titre}</span>
                    </h6>
                  </div>
                </div>
                <div class="row">
                  {cmd.map((Annonces) => (
                    <div className="col-lg-3 col-md-3 col-sm-6">
                      {console.log(Annonces.image_face)}
                      <div className="product__item">
                        <div
                          className="product__item__pic set-bg"
                          // data-setbg={Annonces.images}
                          // src="Images/sardi1.jpg"
                        >
                          <centre>
                            {" "}
                            <img
                              src={Annonces.mouton.image_face}
                              className="product__item__pic set-bg"
                            />
                          </centre>

                          <ul class="product__item__pic__hover">
                            <li>
                              <Link
                                to={{
                                  pathname: "/DetailsCommande",
                                  state: {
                                    id: Annonces,
                                  },
                                }}
                                type="submit"
                              >
                                {" "}
                                <a href="#">
                                  <i class="fa fa-eye"></i>
                                </a>
                              </Link>
                            </li>
                          </ul>
                        </div>
                        <div className="product__item__text">
                          <h6 className="text-danger">
                            {"         " + Annonces.statut}
                          </h6>

                          <h6>
                            {"   Livrer à :      " + Annonces.point_relais}
                          </h6>
                          <h6>
                            {" "}
                            {"         " + Annonces.mouton.prix + "  MAD"}
                          </h6>

                          {/* <h5>{"         " + Annonces.prix + " MAD"}</h5>  */}
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
                {/* <!-- Sheeps Grid Section End --> */}
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default HomeCommandesHistorique;
