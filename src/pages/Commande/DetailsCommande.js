import React, { Component } from "react";
import axios from "axios";
class DetailsCommande extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      Commandes: {},
      image: "",
      redirect: false,
      // checked: false,
      msg: "",
      MsgReste: "",
      checkedValid: false,
      checkedRefus: false,
      checkedValidReste: false,
      checkedRefusReste: false,

      // display: "none",
    };
    this.onClickImageBoucle = this.onClickImageBoucle.bind(this);
    this.onClickImageProfile = this.onClickImageProfile.bind(this);
    this.onClickImageFace = this.onClickImageFace.bind(this);
    this.handleChangeRefus = this.handleChangeRefus.bind(this);
    this.handleChangeValid = this.handleChangeValid.bind(this);
    this.HandelEnvoyer = this.HandelEnvoyer.bind(this);
    this.onChangeMsg = this.onChangeMsg.bind(this);
    this.HandelEnvoyerReste = this.HandelEnvoyerReste.bind(this);
    this.handleChangeRefusReste = this.handleChangeRefusReste.bind(this);
    this.handleChangeValidReste = this.handleChangeValidReste.bind(this);
    this.onChangeMsgReste = this.onChangeMsgReste.bind(this);
    this.handelLivrer = this.handelLivrer.bind(this);
  }

  onClickImageBoucle() {
    const cmd = this.props.location.state.id;
    this.setState({ image: cmd.mouton.image_boucle });
  }
  onClickImageProfile() {
    const cmd = this.props.location.state.id;
    this.setState({ image: cmd.mouton.image_profile });
  }
  onClickImageFace() {
    const cmd = this.props.location.state.id;
    this.setState({ image: cmd.mouton.image_face });
  }

  handleChangeValid() {
    this.setState({
      checkedRefus: this.state.checkedValid,
      checkedValid: !this.state.checkedValid,
    });
  }

  handleChangeRefus() {
    this.setState({
      checkedValid: this.state.checkedRefus,
      checkedRefus: !this.state.checkedRefus,
    });
  }

  handleChangeValidReste() {
    this.setState({
      checkedRefusReste: this.state.checkedValidReste,
      checkedValidReste: !this.state.checkedValidReste,
    });
  }

  handleChangeRefusReste() {
    this.setState({
      checkedValidReste: this.state.checkedRefus,
      checkedRefusReste: !this.state.checkedRefusReste,
    });
  }

  componentDidMount() {
    const cmd = this.props.location.state.id;

    this.setState({ image: cmd.mouton.image_face });
  }
  onChangeMsg(e) {
    // const n = e.target.name,
    const v = e.target.value;
    this.setState({ msg: v });
  }
  onChangeMsgReste(e) {
    // const n = e.target.name,
    const v = e.target.value;
    this.setState({ MsgReste: v });
  }

  handelLivrer() {
    const idc = this.props.location.state.id._id;
    axios
        .put(
          "http://127.0.0.1:8000/api/commande/" + idc,
          {
            statut: "livré",
            
          },
          {
            headers: { "Content-Type": "application/json" },
          }
        )
        .then((res) => {
          this.props.history.push("/HomeCommandesHistorique");
        });
  }

  HandelEnvoyer() {
    const idc = this.props.location.state.id._id;
    if (this.state.checkedValid == true) {
      console.log(idc);
      axios
        .put(
          "http://127.0.0.1:8000/api/commande/" + idc,
          {
            statut: "en attente de paiement du reste",
            feedback_avance: "validé",
          },
          {
            headers: { "Content-Type": "application/json" },
          }
        )
        .then((res) => {
          this.props.history.push("/HomeCommandeValidation");
          
          
        });
    }
    // cmd:"en attente de paiement avance", cmd:"en attente de paiement du reste",   / en attente de validation reçu /  /validé /reçu avance refusé
    // cmd :"réservé" / "vendu" // "disponible"
    if (this.state.checkedRefus == true) {
      axios
        .put(
          "http://127.0.0.1:8000/api/commande/" + idc,
          {
            statut: "reçu avance refusé ",
            reçu_avance: null,
            msg_refus_avance: this.state.msg,
            feedback_avance: "refusé",
            date_creation: new Date(),
          },
          {
            headers: { "Content-Type": "application/json" },
          }
        )
        .then((res) => {
          this.props.history.push("/HomeCommandeValidation");
        });
    }
  }

  HandelEnvoyerReste() {
    const idc = this.props.location.state.id._id;
    const idm = this.props.location.state.id.id_mouton;
    console.log(idc);
    if (this.state.checkedValidReste == true) {
      // console.log(idc);
      axios
        .put(
          "http://127.0.0.1:8000/api/commande/" + idc,
          {
            statut: "validé",
            feedback_reçu_montant_restant: "validé",
          },
          {
            headers: { "Content-Type": "application/json" },
          }
        )
        .then((res) => {
          axios.put(
            "http://127.0.0.1:8000/api/mouton/" + idm,
            {
              statut: "vendu",
            },
            {
              headers: { "Content-Type": "application/json" },
            }
          );
          this.props.history.push("/HomeCommandeValidation");
        });
    }
    // cmd:"en attente de paiement avance", cmd:"en attente de paiement du reste",   / en attente de validation reçu /  /validé /reçu avance refusé
    // cmd :"réservé" / "vendu" // "disponible"
    if (this.state.checkedRefusReste == true) {
      axios
        .put(
          "http://127.0.0.1:8000/api/commande/" + idc,
          {
            statut: "reçu reste refusé",
            reçu_montant_restant: null,
            msg_reçu_reste: this.state.MsgReste,
            feedback_reçu_montant_restant: "refusé",
            date_creation: new Date(),
          },
          {
            headers: { "Content-Type": "application/json" },
          }
        )
        .then((res) => {
          this.props.history.push("/HomeCommandeValidation");
        });
    }
  }

  render() {
    const commandes = this.props.location.state.id;
    return (
      <div>
        <section class="product-details spad">
          <div class="container">
            <div className="row">
              <div class="col-lg-6 col-md-6">
                <div class="product__details__pic">
                  <div class="product__details__pic__item">
                    <img
                      class="product__details__pic__item--large"
                      src={this.state.image}
                      alt=""
                    />
                  </div>

                  <div className="row">
                    <div className="container">
                      <div className="col-lg-12 col-md-12">
                        <img
                          className="col-lg-4 col-md-4"
                          // data-imgbigurl="Images/1.jpg"
                          src={commandes.mouton.image_boucle}
                          alt=""
                          onClick={this.onClickImageBoucle}
                        />
                        <img
                          className="col-lg-4 col-md-4"
                          // data-imgbigurl="Images/1.jpg"
                          src={commandes.mouton.image_face}
                          alt=""
                          onClick={this.onClickImageFace}
                        />
                        <img
                          className="col-lg-4 col-md-4"
                          // data-imgbigurl="Images/1.jpg"
                          src={commandes.mouton.image_profile}
                          alt=""
                          onClick={this.onClickImageProfile}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-md-6">
                <div className="product__details__text">
                  <h3>Détails commande </h3>
                  <h4>
                    Réf : <span>{commandes._id} </span>
                  </h4>

                  <h4>Mouton livré ? {"  "}{commandes.statut === "validé" ? (<button className="site-btn" onClick={this.handelLivrer}> Oui </button>):null}</h4>

                  <div class="product__details__price">{commandes.statut}</div>
                  
                  {/* <div class="product__details__price">
                    {commandes.mouton.prix + "  MAD"}
                  </div> */}
                  <ul>
                    <li>
                      <b>Effectuée le </b>
                      <span>{commandes.date_creation.toLocaleString()}</span>
                    </li>
                    <li>
                      <b>Boucle</b> <span>{commandes.mouton.boucle}</span>
                    </li>
                    <li>
                      <b>Race</b> <span>{commandes.mouton.race}</span>
                    </li>
                    <li>
                      <b>Poids</b> <span>{commandes.mouton.poids} Kg</span>
                    </li>
                    <li>
                      <b>Age</b> <span>{commandes.mouton.age} mois</span>
                    </li>

                    <li>
                      <b>Avance</b>
                      <span>{commandes.mouton.avance} MAD</span>
                    </li>
                    <li>
                      <b>Eleveur</b>
                      {commandes.eleveur.nom + " " + commandes.eleveur.prenom}
                    </li>
              


                    <li className="bg-ligh text-danger h6 center">
                      <b>Prix total</b>
                      {commandes.mouton.prix} MAD
                    </li>

                    <li className="bg-ligh text-danger h6 center">
                      <b>A livrer</b>
                      La veille de l'Aid
                    </li>
                    <li className="bg-ligh text-danger h6 center">
                      <b>Au point de relais </b>
                      <span>{commandes.point_relais}</span>
                    </li>

                    <li className="bg-ligh text-danger h6 center">
                      <b>Heure de livraison</b>
                      <span className="bg-ligh text-danger h6 center">
                        Contacter l'acheteur pour fixer l'heure exacte
                      </span>
                    </li>
                    <div className="bg-light">
                    <li>
                      <b>Client</b>
                      {commandes.consommateur.nom + " " + commandes.consommateur.prenom}
                    </li>
                    <li>
                      <b>Numéro de téléphone </b>
                      {commandes.consommateur.tel }
                    </li>
                    <li>
                      <b>Email du client</b>
                      {commandes.consommateur.email }
                    </li> </div>
                  </ul>
                </div>
              </div>
            </div>

            <br></br>

            <div className="row">
              <div className="col-lg-6 col-md-6">
                <div className="product__details__text">
                  <br></br>
                  <br></br>
                  <h4>Détails paiement avance</h4>
                  <ul>
                    <div class="product__details__pic">
                      <div class="product__details__pic__item">
                        <img
                          class="product__details__pic__item--large"
                          src={commandes.reçu_avance}
                          alt=""
                        />
                      </div>
                    </div>
                  </ul>
                </div>
              </div>

              <div className="col-lg-6 col-md-6">
                <div className="product__details__text">
                  <br></br>
                  <br></br>
                  <h4>Détails paiement reste du montant</h4>
                  <ul>
                    <div class="product__details__pic">
                      <div class="product__details__pic__item">
                        <img
                          class="product__details__pic__item--large"
                          src={commandes.reçu_montant_restant}
                          alt=""
                        />
                      </div>
                    </div>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default DetailsCommande;
