import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
class HomeCommandeValidation extends Component {
  constructor() {
    super();

    this.state = {
      Commandes: [],
      redirect: false,
    };
  }

  componentDidMount() {
    if (!localStorage.getItem("usertoken")) {
      this.props.history.push("/login");
    } else {
    const idt = localStorage.getItem("usertoken");

    axios
      .get("http://127.0.0.1:8000/api/commandes/" + idt, {
        headers: {
          "Content-Type": "application/json",
        },
      })

      .then((res) => {
        this.setState({
          Commandes: res.data,
        });
      });
  } }


//  const json2array(json){
//     var result = [];
//     var keys = Object.keys(json);
//     keys.forEach(function(key){
//         result.push(json[key]);
//     });
//     return result;
// }

  render() {
    // cmd:"en attente de paiement avance", cmd:"en attente de paiement du reste",   / en attente de validation reçu /  /validé /refusé
    // cmd :"réservé" / "vendu" // "disponible"
    var cmd = this.state.Commandes.filter(
      (Commandes) => (Commandes.statut == "en attente de validation avance") ||(Commandes.statut == "en attente de validation reste")
    );
    //  var cmd =  this.state.Commandes;
    let titre;
    if (cmd.length == 1 || cmd.length == 0) {
      titre = <h6><span>{cmd.length}</span> Commande à valider {" "}</h6>;
    } else {
      titre= <h6><span>{cmd.length}</span> Commandes  à valider {" "}</h6>;
    }
    

    console.log(cmd);

    return (
      <div>
        <center>
          <nav class="header__menu filter__item">
            <ul>
              <li className="active">
                <a href="/HomeCommandeValidation"> Commandes à valider</a>
              </li>
              <li>
                <a href="/HomeCommandePrisEnCharge">
                  {" "}
                  Commandes à prendre en charge
                </a>
              </li>
              <li>
                <a href="/HomeCommandesHistorique"> Historique des commandes</a>
              </li>
            </ul>
          </nav>
        </center>

        <section className="product spad">
          <div className="container">
            {/* <h4 class="latest-product__item">Mes commandes</h4> */}
            <div className="row">
              <div className="col-lg-10 col-md-10">
                {/*<!-- Sheeps Grid Section Begin --> */}
                <div className="filter__found text-left">
                    <h6>
                      <span>{titre}</span> 
                    </h6>
                  </div>
                <div class="row">
                  {cmd.map((Annonces) => (
                    <div className="col-lg-4 col-md-4 col-sm-6">
                      {console.log(Annonces.image_face)}
                      <div className="product__item">
                        <div className="product__item__pic set-bg">
                          <centre>
                            {" "}
                            <img
                              src={Annonces.mouton.image_face}
                              className="product__item__pic set-bg"
                            />
                          </centre>

                          <ul class="product__item__pic__hover">
                            <li>
                              <Link
                                to={{
                                  pathname: "/ValidationDetailsCommande",
                                  state: {
                                    id: Annonces,
                                  },
                                }}
                                type="submit"
                              >
                                {" "}
                                <a href="#">
                                  <i class="fa fa-eye"></i>
                                </a>
                              </Link>
                            </li>
                          </ul>
                        </div>
                        <div className="product__item__text">
                          <h6 className="text-danger">
                            {"         " + Annonces.statut}
                          </h6>

                          <h6>
                            {"   Livrer à :      " + Annonces.point_relais}
                          </h6>
                          <h6>
                            {" "}
                            {"         " + Annonces.mouton.prix + "  MAD"}
                          </h6>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
                {/* <!-- Sheeps Grid Section End --> */}
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default HomeCommandeValidation;
