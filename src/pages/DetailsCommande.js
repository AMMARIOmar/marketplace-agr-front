import React, { Component } from "react";

class DetailsCommande extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      Commandes: {
        Nboucle: "122554",
        prix: "3100 Dh",
        Race: "Sardi",
        poids: "58kg",
        Eleveur: "Mohamed Erraji",
        image: "Images/Sardi3.jpg",
        client: "mohamed talmssi",
        avance: "400dh",
        dateAjout: "20/06/2020",
        dateLiv: "27/07/2020",
      },

      redirect: false,
    };
  }
  render() {
    return (
      <div>
        <section class="product-details spad">
          <div class="container">
            <div className="row">
              <div class="col-lg-6 col-md-6">
                <div class="product__details__pic">
                  <div class="product__details__pic__item">
                    <img
                      class="product__details__pic__item--large"
                      src="Images/1.jpg"
                      alt=""
                    />
                  </div>
                  <div className="product__details__pic__slider owl-carousel">
                    <img
                      data-imgbigurl="Images/1.jpg"
                      src="Images/1.jpg"
                      alt=""
                    />
                    <img
                      data-imgbigurl="Images/1.jpg"
                      src="Images/1.jpg"
                      alt=""
                    />
                    <img
                      data-imgbigurl="Images/1.jpg"
                      src="Images/1.jpg"
                      alt=""
                    />
                    <img
                      data-imgbigurl="Images/1.jpg"
                      src="Images/1.jpg"
                      alt=""
                    />
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-md-6">
                <div className="product__details__text">
                  <h3>Details commande Numéro </h3>
                  {/* <div className="product__details__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                             <span>(18 reviews)</span> 
                        </div> */}
                  <div class="product__details__price">
                    {this.state.Commandes.prix}
                  </div>

                  {/* <div class="product__details__quantity">
                            <div class="quantity">
                                <div class="pro-qty">
                                    <input type="text" value="1"/>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="primary-btn">ADD TO CARD</a>
                        <a href="#" class="heart-icon"><span class="icon_heart_alt"></span></a> */}
                  <ul>
                    <li>
                      <b>Race</b> <span>{this.state.Commandes.Race}</span>
                    </li>
                    <li>
                      <b>Poids</b> <span>{this.state.Commandes.poids} </span>
                    </li>
                    <li>
                      <b>Date d'ajout</b>{" "}
                      <span>{this.state.Commandes.dateAjout}</span>
                    </li>
                    <li>
                      <b>Avance</b>
                      <span>{this.state.Commandes.avance}</span>
                    </li>
                    <li>
                      <b>Prix total</b>
                      <span>{this.state.Commandes.prix}</span>
                    </li>
                    <li>
                      <b>Nom propriétaire</b>
                      {this.state.Commandes.client}
                    </li>
                    <li>
                      <b>Téléphone éleveur</b>+21212548796
                    </li>
                    <li className="bg-ligh text-danger h6 center">
                      <span className="bg-ligh text-danger h6 center">
                        à livrer le 20/06/2020 à 16h Au point de relais :Rte
                        tailet Oujda
                      </span>{" "}
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="product__details__tab">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                      <a
                        class="nav-link active"
                        data-toggle="tab"
                        href="#tabs-1"
                        role="tab"
                        aria-selected="true"
                      ></a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                      <div class="product__details__tab__desc">
                        <h6>
                          Avance payée {"                       "}
                          <div class="form-check form-check-inline">
                            <input
                              class="form-check-input"
                              type="checkbox"
                              id="inlineCheckbox2"
                              value="option2"
                            />
                            <label
                              class="form-check-label"
                              for="inlineCheckbox2"
                            >
                              On
                            </label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input
                              class="form-check-input"
                              type="checkbox"
                              id="inlineCheckbox1"
                              value="option2"
                            />
                            <label
                              class="form-check-label"
                              for="inlineCheckbox1"
                            >
                              Off
                            </label>
                          </div>
                        </h6>
                        <span>
                          {" "}
                          <h6>
                            Reste du montant payé {"                       "}
                            <div class="form-check form-check-inline">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox2"
                                value="option2"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox2"
                              >
                                On
                              </label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox1"
                                value="option2"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox1"
                              >
                                Off
                              </label>
                            </div>
                          </h6>
                        </span>
                        <span>
                          {" "}
                          <h6>
                            Mouton livré {"                       "}
                            <div class="form-check form-check-inline">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox2"
                                value="option2"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox2"
                              >
                                On
                              </label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox1"
                                value="option2"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox1"
                              >
                                Off
                              </label>
                            </div>
                          </h6>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default DetailsCommande;
