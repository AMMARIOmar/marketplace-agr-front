import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class HomeUsers extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      techniciens: [],
      redirect: false,
    };
  }

  componentDidMount() {
    axios
    .get("http://127.0.0.1:8000/api/techniciens/", {
      headers: {
        // "x-access-token": token, // the token is a variable which holds the token
      },
    })
    .then((res) => {
      //console.log(res);
      this.setState({
        techniciens: res.data,
      });
    });
  }

  handleValidate(clicked_id) {     
    if (window.confirm(`Compte technicien validé`)) {
      axios
        .put("http://127.0.0.1:8000/api/technicien/" + clicked_id, {"statut":"validé"}, {
            // "x-access-token": token, // the token is a variable which holds the token
            headers: { "Content-Type": "application/json" },
        }).then((res) => {
          //console.log(res);
          const to = res.data.technicien.email;
          //console.log(to);
          const content = "Votre compte a été validé par l'administrateur ANOC.";
          const subject = "Compte marketplace validé";
          axios.post("http://127.0.0.1:8000/api/sendmail/" + to+"/"+content+"/"+subject, {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              // "Access-Control-Allow-Origin": "*",
            },
          }).then((resultat) => { console.log(resultat)});
          window.location.reload();
        });
       
    } 
    else {
    }
  }

  handleRefus(clicked_id) {     
    if (window.confirm(`Compte technicien refusé`)) {
      axios
        .put("http://127.0.0.1:8000/api/technicien/" + clicked_id, {"statut":"refusé"}, {
            // "x-access-token": token, // the token is a variable which holds the token
            headers: { "Content-Type": "application/json" },
        }).then((res) => {
          //console.log(res);
          const to = res.data.technicien.email;
          //console.log(to);
          const content = "Votre compte a été refusé par l'administrateur ANOC.";
          const subject = "Compte marketplace refusé";
          axios.post("http://127.0.0.1:8000/api/sendmail/" + to+"/"+content+"/"+subject, {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              // "Access-Control-Allow-Origin": "*",
            },
          }).then((resultat) => { console.log(resultat)});
          window.location.reload();
        });
    } 
    else {
    }
  }

  render() {
    var tech = this.state.techniciens.filter(
      (techniciens) => (techniciens.statut == "à valider")
    );
    //console.log(tech);
    
    let titre;
    if (tech.length == 1 || tech.length == 0) {
      titre = <h6><span>{tech.length}</span> Compte technicien à valider {" "}</h6>;
    } else {
      titre= <h6><span>{tech.length}</span> Comptes techniciens à valider {" "}</h6>;
    }
    return (
      <div>
        {" "}
        <center>
          <div class="col-lg-10 col-md-10">
            <div className="filter__item">
              <div className="row">
                <div className="col-lg-4 col-md-5"></div>
                <div className="col-lg-12 col-md-12">
                  <div className="filter__found text-left">
                    {titre}
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              {tech.map((technicien) => (
                <div class="col-lg-4 col-md-6 col-sm-6" >
                  <div class="product__item" style={{ backgroundColor: "#DCDCDC" }}>
                    <br></br>
                    <div
                      class="product__item__pic set-bg"
                      data-setbg="Images/Eleveur.jpg">
                      <img
                        src="Images/profilEleveur.jpg"
                        className="product__item__pic set-bg" />
                    </div>
                    <div class="product__item__text">
                      <h6>
                          <a href="#">
                            {"         " +
                              technicien.nom +
                              "         " +
                              technicien.prenom}
                          </a>
                      </h6>
                      <h6>{technicien.tel}</h6>
                      <h6>{technicien.email}</h6>
                      <h6>{technicien.groupement}</h6>
                      <h5>{technicien.statut}</h5>
                    </div>
                    <div>
                      <button className="site-btn" 
                              id={technicien._id}                           
                              onClick={(e) =>
                                this.handleValidate(e.currentTarget.id)
                              }>Valider
                      </button> {"   "}
                      <button className="site-btn"
                              id={technicien._id}
                              onClick={(e) =>
                                this.handleRefus(e.currentTarget.id)
                              }>Refuser
                      </button>
                    </div>
                    <br></br>
                  </div>
                </div>
              ))}
            </div>
          </div>{" "}
        </center>
      </div>
    );
  }
}

export default HomeUsers;
