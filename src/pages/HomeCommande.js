import React, { Component } from 'react';
import axios from "axios";
class HomeCommande extends Component {
    constructor() {
        super();
        // let redirect = false;
        this.state = {
          Annonces: [
           
            //   
          ],
          redirect: false,
        };
      }

      componentDidMount() {
        // const token = localStorage.getItem("usertoken");
        // if (!token) {
        //   this.props.history.push("/login");
        // } else {
          // console.log(token);
          axios
            .get("http://127.0.0.1:8000/api/commande", {
              headers: {
                // "x-access-token": token, // the token is a variable which holds the token
                "Content-Type": "application/json",
              },
              params: {
                statut: "en attente de validation reçu_reste",
                order_by: "date_creation",
                order_mode: "asc",
              },
            })
    
            .then((res) => {
              // console.log(res.data);
              this.setState({
                Annonces: res.data,
              });

            });
            axios
            .get("http://127.0.0.1:8000/api/commande", {
              headers: {
                // "x-access-token": token, // the token is a variable which holds the token
                "Content-Type": "application/json",
              },
              params: {
                // statut: "en attente de validation reçu_avance",
                statut: "reservé",
                order_by: "date_creation",
                order_mode: "asc",
              },
            })
    
            .then((res) => {
              // console.log(res.data);
              this.setState({
                Annonces: this.state.Annonces.concat(res.data),
              });

            });

        }
      
  



    render() {
        return (
            <div>
             
                {/* <div className="featured__controls"> */}
                <section className="hero">
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-9">
                        <div className="hero__search">
                          <div className="hero__search__form">
                            <form action="#">
                              <div className="hero__search__categories">
                                éleveur
                                <span className="arrow_carrot-down"></span>
                              </div>
                              <input
                                type="text"
                                placeholder=""
                              />
                              <button type="submit" className="site-btn">
                                CHERCHER
                              </button>
                            </form>
                          </div>
                          {/* <div className="hero__search__phone">
                              <div className="hero__search__phone__text"></div>
                            </div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </section> 
                <section className="categories">
          <div className="container">
            <div className="row">
              <div className="categories__slider owl-carousel ">
                {this.state.Annonces.map((Annonces) => (
                  <a href="#" className="latest-product__item col-lg-6">
                    <div className="latest-product__item__pic">
                      <img src={Annonces.mouton.images} alt="" />
                    </div>
                    <div className="latest-product__item__text col-lg-6 ">
                      <h6>
                        <b>Numéro de boucle {Annonces.mouton.boucle}</b>
                      </h6>
                      <h6>
                        <b>Prix</b>
                        {"         " + Annonces.mouton.prix +"         " } <b className="text-right">Race</b> {"         " + Annonces.mouton.race}
                      </h6>
                      {/* <h6>
                        <b>Race</b>
                        {"         " + Annonces.Race}
                      </h6> */}
                      <h6>
                        <b>Poids</b>
                        {"         " + Annonces.mouton.poids +"         " }<b className="text-right">Avance</b>
                        {"         " + Annonces.mouton.avance}
                      </h6>
                      {/* <h6>
                        <b>Avvance</b>
                        {"         " + Annonces.avance}
                      </h6> */}
                      <h6>
                        <b>Eleveur</b> {Annonces.eleveur.nom}
                      </h6>
                      <h6>
                        {/* <b>Client</b> {Annonces.consommateur.nom} */}
                      </h6>
                      <center>
                        <a className=" btn btn-success " href="/DetailsCommande">
                          {/* <i className="fa fa-wrench"></i> */}
                          Consulter
                        </a>

                      </center>
                    </div>
                  </a>
                ))}
              </div>
            </div>
          </div>
        </section>

            </div>
        );
    }
}

export default HomeCommande;