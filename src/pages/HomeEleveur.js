import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
class HomeEleveur extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      Eleveurs: [],
      redirect: false,
    };
  }
  componentDidMount() {
    
    axios
      .get("http://127.0.0.1:8000/api/eleveurs", {
        headers: {
          // "x-access-token": token, // the token is a variable which holds the token
        },
      })
      .then((res) => {
        console.log(res);
        this.setState({
          Eleveurs: res.data,
        });
      });
  }
  render() {
    return (
      <div>
        {/* <div className="featured__controls"> */}
        <section className="hero">
          <div className="container">
            <div className="row">
              <div className="col-lg-9">
                <div className="hero__search">
                  <div className="hero__search__form">
                    <form action="#">
                      <div className="hero__search__categories">
                        éleveur
                        <span className="arrow_carrot-down"></span>
                      </div>
                      <input type="text" placeholder="" />
                      <button type="submit" className="site-btn">
                        CHERCHER
                      </button>
                    </form>
                  </div>
                  {/* <div className="hero__search__phone">
                              <div className="hero__search__phone__text"></div>
                            </div> */}
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="categories">
          <div className="container">
            <div className="row">
              <div className="categories__slider owl-carousel ">
                {this.state.Eleveurs.map((Eleveur) => ( <Link
                          className="btn "
                          to={{
                            pathname: "/DetailsEleveur",
                            state: {
                              id: Eleveur._id,
                            },
                          }}
                        >
                  <a href="#" className="latest-product__item col-lg-6">
                    <div className="latest-product__item__pic">
                      <img src="Images/Eleveur.jpg" alt="" />
                    </div>
                   
                    <div className="latest-product__item__text col-lg-6 ">
                      <h6>
                        <b>Nom </b>
                        {"         " + Eleveur.nom + "         "+ Eleveur.prenom }
                      </h6>

                      <h6>
                        <b>Adresse</b>
                        {"         " + Eleveur.adresse + "         "}
                      </h6>

                      <h6>
                        <b>Téléphone</b> {Eleveur.tel}
                      </h6>

                      <center>
                        <button className="btn btn-success ">
                          <i className="fa fa-wrench"></i>
                        </button>
                        {"                   "}
                        <button href="#" className="btn btn-danger">
                          <i className="fa fa-trash"></i>
                        </button>
                      </center>
                    </div>
                  </a></Link>
                ))}
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default HomeEleveur;
