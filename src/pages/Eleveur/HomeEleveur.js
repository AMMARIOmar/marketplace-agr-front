import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
class HomeEleveur extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      Eleveurs: [],
      redirect: false,
    };
  }
  componentDidMount() {

    const idt = localStorage.getItem("usertoken");
    // const token = localStorage.getItem("usertoken");
    
    if (!idt) {
      
      this.props.history.push("/login");
    } 
else{
    axios
      .get("http://127.0.0.1:8000/api/technicien/" + idt + "/eleveurs", {
        headers: {
          // "x-access-token": token, // the token is a variable which holds the token
        },
      })
      .then((res) => {
        console.log(res);
        this.setState({
          Eleveurs: res.data,
        });
      });
  }}
  handelDelete(clicked_id) {
    // const token = localStorage.getItem("usertoken");
    const idt = localStorage.getItem("usertoken");
    // if (!token) {
    //   this.props.history.push("/login");
    // } else {
    if (window.confirm(`DELETE Eleveur  ... !`)) {
      axios
        .delete("http://127.0.0.1:8000/api/eleveur/" + clicked_id, {
          headers: {
            // "x-access-token": token, // the token is a variable which holds the token
          },
        })
        .then((res) => {
          axios.put(
            "http://127.0.0.1:8000/api/technicien/" +
              idt +
              "/eleveur/" +
              clicked_id,
            {
              headers: {
                // "x-access-token": token, // the token is a variable which holds the token
              },
            }
          );
          // this.props.history.push("/Eleveurs");
          window.location.reload();
          // this.componentDidMount();
        });
    } else {
    }
  }
  render() {
    return (
      <div>
        <center>
          <div className="col-lg-10 col-md-10">
            <div className="filter__item">
              <div className="row">
                <div className="col-lg-4 col-md-4">
                  <div className="filter__found text-left">
                    <h6>
                      <span>{this.state.Eleveurs.length}</span> Eleveurs
                    </h6>
                  </div>
                  </div>
                  <div className="col-lg-8 col-md-3 text-left">
                    <div className="filter__option">
                      <a href="/AddEleveur">
                        {" "}
                        <button className="site-btn ">Ajouter éleveur</button>
                      </a>
                    </div>
                  </div>
              </div>
            </div>
            <div class="row">
              {this.state.Eleveurs.map((Eleveur) => (
              
                <div class="col-lg-3 col-md-6 col-sm-6">
                 
                  {/* {  console.log(Eleveur.eleveur.nom)}  */}
                  <div class="product__item">
                    <div
                      class="product__item__pic set-bg"
                      data-setbg="Images/Eleveur.jpg"
                    >
                      <img
                        src="Images/profilEleveur.jpg"
                        className="product__item__pic set-bg"
                      />
                      <ul class="product__item__pic__hover">
                        <li>
                          <Link
                            id={Eleveur.id_eleveur}
                            onClick={(e) =>
                              this.handelDelete(e.currentTarget.id)
                            }
                          >
                            <a href="#">
                              <i class="fa fa-trash"></i>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to={{
                              pathname: "/MoutonsEleveur",
                              state: {
                                id: {
                                  id: Eleveur.id_eleveur,
                                  nom: Eleveur.eleveur.nom,
                                  prenom:Eleveur.eleveur.prenom,
                                },
                              },
                            }}
                          >
                            {" "}
                            <a href="#">
                              <i class="fa fa-home"></i>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to={{
                              pathname: "/UpdateEleveur",
                              state: {
                                id: Eleveur.id_eleveur,
                              },
                            }}
                          >
                            {" "}
                            <a href="#">
                              <i class="fa fa-wrench"></i>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to={{
                              pathname: "/DetailsEleveur",
                              state: {
                                id: Eleveur.id_eleveur,
                              },
                            }}
                          >
                            {" "}
                            <a href="#">
                              <i class="fa fa-eye"></i>
                            </a>
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div class="product__item__text">
                      <h6>
                        <Link
                          className="btn "
                          to={{
                            pathname: "/DetailsEleveur",
                            state: {
                              id: Eleveur.id_eleveur,
                            },
                          }}
                        >
                          {" "}
                          <a href="#">
                            {"         " +
                              Eleveur.eleveur.nom +
                              "         " +
                              Eleveur.eleveur.prenom}
                          </a>
                        </Link>
                      </h6>
                      <h5>{Eleveur.eleveur.tel}</h5>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            {/* <div class="product__pagination">
                <a href="#">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">
                  <i class="fa fa-long-arrow-right"></i>
                </a>
              </div> */}
          </div>{" "}
        </center>
      </div>
    );
  }
}

export default HomeEleveur;
