import React, { Component } from "react";
import axios from "axios";

class UpdateEleveur extends Component {
  constructor() {
    super();
    this.state = {
      dataa: {},
      Eleveur: {},
    };
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    const n = e.target.name,
      v = e.target.value;

    this.setState({ Eleveur: Object.assign(this.state.Eleveur, { [n]: v }) });
  }

  componentDidMount() {
    const idz = this.props.location.state.id;
    axios
      .get("http://127.0.0.1:8000/api/eleveur/" + idz, {
        headers: {
          // "x-access-token": token, // the token is a variable which holds the token
        },
      })
      .then((res) => {
        this.setState({
          Eleveur: res.data,
        });
      });
  }

  handlePut = (e) => {
    e.preventDefault();

    const idz = this.props.location.state.id;
    axios
      .put("http://127.0.0.1:8000/api/eleveur/" + idz, this.state.Eleveur, {
        headers: { "Content-Type": "application/json" },
      })
      .then((res) => {
        this.props.history.push("/Eleveurs");
      });
  };

  render() {
    return (
      <div>
        <div className="contact-form spad">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="contact__form__title">
                  <h2>Modifier éleveur</h2>
                </div>
              </div>
            </div>
            {/* '_id','civilisation','nom','prenom', 'tel','email','adresse','region','rib', 'moutons','id_user' */}
            <form action="#" onSubmit={this.handlePut} name="addEleveur">
              <div className="row">
              <div className="col-lg-6 col-md-6">
                  <span>CIN </span>
                  <input
                    type="text"
                    placeholder="CIN"
                    onChange={this.onChange}
                    name="cin"
                    defaultValue={this.state.Eleveur.cin}
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Nom </span>
                  <input
                    type="text"
                    placeholder="Nom"
                    onChange={this.onChange}
                    name="nom"
                    defaultValue={this.state.Eleveur.nom}
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span> Prénom </span>
                  <input
                    type="text"
                    placeholder="Prénom"
                    onChange={this.onChange}
                    name="prenom"
                    defaultValue={this.state.Eleveur.prenom}
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span> Numéro de téléphone </span>
                  <input
                    type="text"
                    placeholder="Numéro de téléphone"
                    onChange={this.onChange}
                    name="tel"
                    defaultValue={this.state.Eleveur.tel}
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span> Email </span>
                  <input
                    type="email"
                    placeholder="Email"
                    onChange={this.onChange}
                    name="email"
                    defaultValue={this.state.Eleveur.email}
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span> Adresse </span>
                  <input
                    type="text"
                    placeholder="Adresse"
                    onChange={this.onChange}
                    name="adresse"
                    defaultValue={this.state.Eleveur.adresse}
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span> Ville </span>
                  <input
                    type="text"
                    placeholder="Ville"
                    onChange={this.onChange}
                    name="region"
                    defaultValue={this.state.Eleveur.region}
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span> Numéro de RIB </span>
                  <input
                    type="text"
                    placeholder="Numéro de RIB"
                    onChange={this.onChange}
                    name="rib"
                    defaultValue={this.state.Eleveur.rib}
                    required
                  />
                </div>

                <div className="col-lg-6 ">
                  <span> Photo de profile </span>
                  <input
                    type="file"
                    placeholder=""
                    onChange={this.handleChangeImageProfile}
                  />
                </div>
                <div className="col-lg-12 text-center">
                  <button type="submit" className="site-btn">
                    Modifier
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default UpdateEleveur;
