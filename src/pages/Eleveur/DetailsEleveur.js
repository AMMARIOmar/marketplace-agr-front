import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
class DetailsEleveur extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      Eleveur: {},

      redirect: false,
    };
  }

  componentDidMount() {
    const idz = this.props.location.state.id;
    axios
      .get("http://127.0.0.1:8000/api/eleveur/" + idz, {
        headers: {
          // "x-access-token": token, // the token is a variable which holds the token
        },
      })
      .then((res) => {
        this.setState({
          Eleveur: res.data,
        });
      });
  }
  handelDelete(clicked_id) {
    // const token = localStorage.getItem("usertoken");
    // if (!token) {
    //   this.props.history.push("/login");
    // } else {
    if (window.confirm(`DELETE Eleveur  ... !`)) {
      axios
        .delete("http://127.0.0.1:8000/api/eleveur/" + clicked_id, {
          headers: {
            // "x-access-token": token, // the token is a variable which holds the token
          },
        })
        .then((res) => {
          // const applications = this.state.applications.filter(
          //   c => c._id !== clicked_id
          // );
          // this.setState({ applications });
          this.props.history.push("/Eleveurs");
        });
    } else {
    }
  }

  render() {
    return (
      <div>
        <section class="product-details spad">
          <div class="container">
            <div className="row">
              <div class="col-lg-6 col-md-6">
                <div class="product__details__pic">
                  <div class="product__details__pic__item">
                    <img
                      class="product__details__pic__item--large"
                      src="Images/Eleveur.jpg"
                      alt=""
                    />
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-md-6">
                <div className="product__details__text">
                  <h3>Details éleveur </h3>
                  {/* <div className="product__details__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                             <span>(18 reviews)</span> 
                        </div> */}
                  {/* <div class="product__details__price">
                    {this.state.Eleveur.prix}
                  </div> */}

                  {/* <div class="product__details__quantity">
                            <div class="quantity">
                                <div class="pro-qty">
                                    <input type="text" value="1"/>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="primary-btn">ADD TO CARD</a>
                        <a href="#" class="heart-icon"><span class="icon_heart_alt"></span></a> */}
                  <ul>
                    <li>
                      <b>Nom</b> <span>{this.state.Eleveur.civilisation+"  "}</span>
                      {"           "}
                      <span>{this.state.Eleveur.prenom+"  "}</span><span>{this.state.Eleveur.nom}</span>
                    </li>
                    <li>
                      <b>Adresse</b> <span>{this.state.Eleveur.adresse} </span>
                    </li>
                    <li>
                      <b>Numéro de Téléphone</b>{" "}
                      <span>{this.state.Eleveur.tel}</span>
                    </li>
                    <li>
                      <b>Email</b>
                      <span>{this.state.Eleveur.email}</span>
                    </li>
                    <li>
                      <b>CIN</b> <span>{this.state.Eleveur.cin} </span>
                    </li>
                    <li>
                      <b>Numéro de RIB</b>
                      <span>{this.state.Eleveur.rib}</span>
                    </li>
                    <li className="text-center">
                      <Link
                        id={this.state.Eleveur._id}
                        onClick={(e) => this.handelDelete(e.currentTarget.id)}
                      >
                        <button className="btn btn-danger ">Supprimer</button>{" "}
                      </Link>{" "}
                      {"           "}{" "}
                      <Link
                        to={{
                          pathname: "/UpdateEleveur",
                          state: {
                            id: this.state.Eleveur._id,
                          },
                        }}
                      >
                        <button className="btn btn-success center">
                          Modifier
                        </button>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default DetailsEleveur;
