import React, { Component } from "react";
import axios from "axios";
const qs = require("qs");
class AddEleveur extends Component {
  constructor() {
    super();
    this.state = {
      selectedFile: "",
      dataa: {},
      photo_profil: "",
    };
    this.onChange = this.onChange.bind(this);
    this.handleChangeImageProfile = this.handleChangeImageProfile.bind(this);
  }
  onChange(e) {
    const n = e.target.name,
      v = e.target.value;
    this.setState({ dataa: Object.assign(this.state.dataa, { [n]: v }) });
  }

  handleChangeImageProfile(evt) {
    var dataURL = "";
    var reader = new FileReader();
    var file = evt.target.files[0];
    const scope = this;
    reader.onload = function () {
      dataURL = reader.result;
      console.log(dataURL);
      scope.setState({ photo_profil: dataURL });
    };
    reader.readAsDataURL(file);
    // console.log(this.props.location.state.id)
  }

  // fileSelectedHandler = (event) => {
  //   // console.log(event.target.files[0])
  //   // const fd = new FormData();
  //   // fd.append('photo_profil:', event.target.files[0]);
  //   this.setState({
  //     dataa: Object.assign(this.state.dataa, {
  //       // "photo_profil": fd.value,
  //       photo_profil: event.target.files[0].name,
  //     }),
  //     // dataa: fd,
  //   });
  //   console.log(this.state.dataa);
  // };

  //   load=()=>{
  // const fd=new FormData();
  // fd.append('image',this.state.selectedFile,this.state.selectedFile.name)
  // console.log(fd);
  //   }

  handlePoste = (e) => {
    e.preventDefault();
    // const token = localStorage.getItem("usertoken");
    // const userId = localStorage.getItem("userId");
    // if (!token) {
    //   this.props.history.push("/login");
    // } else {
    // alert(userId);
    // const eleveur = {
    //   dataa: this.state.dataa,
    //   // user: userId,
    //   // status: "Panding",
    // };
    this.setState({
      dataa: Object.assign(this.state.dataa, {
        photo_profil: this.state.photo_profil,
        civilisation:"Monsieur"
      }),
    });

    axios
      .post("http://127.0.0.1:8000/api/eleveur", this.state.dataa, {
        headers: {
          // "Content-Type": "multipart/form-data",
          Accept: "application/json",
          // "type": "formData"
        },
      })
      .then((res) => {
        // console.log(this.state.dataa)
        // alert("Eleveur has been added successfully.");
        // // console.log(this.state.dataa);
        // // document.forms["addEleveur"].reset();
        // // console.log(this.state.dataa);
        // // const fd = new FormData();
        // // fd.append(
        // //   "image",
        // //   this.state.selectedFile,
        // //   this.state.selectedFile.name
        // // );
        // // console.log(fd);
        // this.props.history.push("/Eleveurs");
        // // this.componentDidMount()
        const idt = localStorage.getItem("usertoken");
        console.log(idt);
        // console.log(res.data.objet._id);
        axios
          .put(
            "http://127.0.0.1:8000/api/technicien/" + idt + "/eleveur",
            { id_eleveur: res.data.objet._id },
            {
              headers: { "Content-Type": "application/json" },
            }
          )
          .then((res) => {
            this.props.history.push("/Eleveurs");
          });
      });
  };

  render() {
    return (
      <div>
        <div className="contact-form spad">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="contact__form__title">
                  <h2>Ajouter éleveur</h2>
                </div>
              </div>
            </div>
            {/* encType="multipart/form-data" */}
            {/* '_id','civilisation','nom','prenom', 'tel','email','adresse','region','rib', 'moutons','id_user' */}
            <form action="#" onSubmit={this.handlePoste} name="addEleveur">
              <div className="row">

                
              <div className="col-lg-6 col-md-6">
                  <span>CIN </span>
                  <input
                    type="text"
                    placeholder="CIN"
                    onChange={this.onChange}
                    name="cin"
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Nom </span>
                  <input
                    type="text"
                    placeholder="Nom"
                    onChange={this.onChange}
                    name="nom"
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Prénom </span>
                  <input
                    type="text"
                    placeholder="Prénom"
                    onChange={this.onChange}
                    name="prenom"
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Numéro de téléphone </span>
                  <input
                    type="text"
                    placeholder="Numéro de téléphone"
                    onChange={this.onChange}
                    name="tel"
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Email </span>
                  <input
                    type="email"
                    placeholder="Email"
                    onChange={this.onChange}
                    name="email"
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Adresse</span>
                  <input
                    type="text"
                    placeholder="Adresse"
                    onChange={this.onChange}
                    name="adresse"
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Ville </span>
                  <input
                    type="text"
                    placeholder="Ville"
                    onChange={this.onChange}
                    name="region"
                    required
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Numéro de RIB </span>
                  <input
                    type="text"
                    placeholder="Numéro de RIB"
                    onChange={this.onChange}
                    name="rib"
                    required
                  />
                </div>

                <div className="col-lg-6 ">
                  <span> Photo de profile </span>
                  <input
                    type="file"
                    placeholder=""
                    onChange={this.handleChangeImageProfile}
                  />
                </div>
                <div className="col-lg-12 text-center">
                  <button type="submit" className="site-btn">
                    Ajouter
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AddEleveur;
