import React, { Component } from "react";
import axios from "axios";
import Select from "react-select";
// const options = [
//   { value: "chocolate", label: "Chocolate" },
//   { value: "strawberry", label: "Strawberry" },
//   { value: "vanilla", label: "Vanilla" },
// ];
// age,images
class UpdateMouton extends Component {
  constructor() {
    super();
    this.state = {
      Eleveurs: [],
      dataa: { statut: "disponible", images: "Images/Sardi3.jpg" },
      selectedOption: null,
      selectedOptionRace: null,
      selectedOptionSexe: null,
      options: [],
      optionsSexe: [
        { value: "Male", label: "Male" },
        { value: "Female", label: "Female" },
      ],
      optionsRace: [
        { value: "Sardi", label: "Sardi" },
        { value: "Bargui", label: "Bargui" },
      ],
      mot: {},
      elv: {},
      image_face: "",
      image_profile: "",
      image_boucle: "",
    };
    this.onChange = this.onChange.bind(this);
    this.handleChangeImageFace = this.handleChangeImageFace.bind(this);
    this.handleChangeImageProfile = this.handleChangeImageProfile.bind(this);
    this.handleChangeImageBoucle = this.handleChangeImageBoucle.bind(this);
  }

  handleChangeImageFace(evt) {
    var dataURL = "";
    var reader = new FileReader();
    var file = evt.target.files[0];
    const scope = this;
    reader.onload = function () {
      dataURL = reader.result;
      console.log(dataURL);
      scope.setState({ image_face: dataURL });
    };
    reader.readAsDataURL(file);
    // console.log(this.props.location.state.id)
  }

  handleChangeImageProfile(evt) {
    var dataURL = "";
    var reader = new FileReader();
    var file = evt.target.files[0];
    const scope = this;
    reader.onload = function () {
      dataURL = reader.result;
      console.log(dataURL);
      scope.setState({ image_profile: dataURL });
    };
    reader.readAsDataURL(file);
    // console.log(this.props.location.state.id)
  }

  handleChangeImageBoucle(evt) {
    var dataURL = "";
    var reader = new FileReader();
    var file = evt.target.files[0];
    const scope = this;
    reader.onload = function () {
      dataURL = reader.result;
      console.log(dataURL);
      scope.setState({ image_boucle: dataURL });
    };
    reader.readAsDataURL(file);
    // console.log(this.props.location.state.id)
  }

  onChange(e) {
    const n = e.target.name,
      v = e.target.value;

    this.setState({ dataa: Object.assign(this.state.dataa, { [n]: v }) });
  }
  componentDidMount() {
    const idm = this.props.location.state.id;
    axios
      .get("http://127.0.0.1:8000/api/mouton/" + idm, {
        headers: {
          // "x-access-token": token, // the token is a variable which holds the token
        },
      })
      .then((res) => {
        this.setState({
          mot: res.data.objet,
          elv: res.data.Eleveur[0],
          selectedOption: {
            value: res.data.Eleveur[0]._id,
            label: res.data.Eleveur[0].nom + " " + res.data.Eleveur[0].prenom,
          },
          selectedOptionSexe: {
            value: res.data.objet.sexe,
            label: res.data.objet.sexe,
          },
          selectedOptionRace: {
            value: res.data.objet.race,
            label: res.data.objet.race,
          },
        });
        // console.log(res)
      });
    const idt = localStorage.getItem("usertoken");
    axios
      .get("http://127.0.0.1:8000/api/technicien/" + idt + "/eleveurs", {
        headers: {
          // "x-access-token": token, // the token is a variable which holds the token
        },
      })
      .then((res) => {
        // console.log(res);
        res.data.map((Eleveur) =>
          this.setState({
            options: this.state.options.concat({
              value: Eleveur.eleveur._id,
              label: Eleveur.eleveur.nom + " " + Eleveur.eleveur.prenom,
            }),
          })
        );
      });

    // console.log(this.state.elv);
  }
  handlePut = (e) => {
    e.preventDefault();

    this.setState({
      dataa: Object.assign(this.state.dataa, {
        id_eleveur: this.state.selectedOption.value,
        image_face: this.state.image_face,
        image_profile: this.state.image_profile,
        image_boucle: this.state.image_boucle,
      }),
    });
    const ida = this.props.location.state.id;
    axios
      .put("http://127.0.0.1:8000/api/mouton/" + ida, this.state.dataa, {
        headers: { "Content-Type": "application/json" },
      })
      .then((res) => {
        const ide = this.state.selectedOption.value;
        // console.log(ide);
        const idm = this.props.location.state.id;

        axios
          .put(
            "http://127.0.0.1:8000/api/eleveur/" + ide + "/mouton/" + idm,

            {
              headers: { "Content-Type": "application/json" },
            }
          )
          .then(
            axios
              .put(
                "http://127.0.0.1:8000/api/eleveur/" + ide + "/mouton",
                {
                  id_mouton: idm,
                },
                {
                  headers: { "Content-Type": "application/json" },
                }
              )
              .then((res) => {
                alert("Mouton has been updated successfully.");
                this.props.history.push("/Annonces");
                // document.forms["addMouton"].reset();
              })
          );
      });
  };
  handleChange = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption.value)
    );
  };

  handleChangeSexe = (selectedOptionSexe) => {
    this.setState({ selectedOptionSexe }, () =>
      console.log(`Option selected:`, this.state.selectedOptionSexe.value)
    );
  };

  handleChangeRace = (selectedOptionRace) => {
    this.setState({ selectedOptionRace }, () =>
      console.log(`Option selected:`, this.state.selectedOptionRace.value)
    );
  };

  //'_id','boucle','race', 'sexe','poids','localisation','prix','images','video','description', 'date_ajout', 'avance'

  render() {
    const { selectedOption } = this.state;
    const { options } = this.state;
    const { selectedOptionSexe } = this.state;
    const { optionsSexe } = this.state;
    const { selectedOptionRace } = this.state;
    const { optionsRace } = this.state;

    return (
      <div>
        <div className="contact-form spad">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="contact__form__title">
                  <h2>Modifier annonce mouton</h2>
                </div>
              </div>
            </div>
            <form action="#" onSubmit={this.handlePut} name="addMouton">
              <div className="row">
                <div className="col-lg-6 col-md-6">
                  <span>Numéro de boucle </span>
                  <input
                    type="text"
                    placeholder="Numéro de boucle"
                    name="boucle"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.boucle}
                  />
                </div>
                {/* <div className="col-lg-6 col-md-6">
                  <input
                    type="text"
                    placeholder="Race"
                    name="race"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.race}
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <input
                    type="text"
                    placeholder="Sexe"
                    name="sexe"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.sexe}
                  />
                </div> */}

                <div className="col-lg-6 col-md-6">
                  <span>Poids (Kg) </span>
                  <input
                    type="text"
                    placeholder="Poids"
                    name="poids"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.poids}
                  />
                </div>

                {/* <div className="col-lg-6 col-md-6">
                  <input
                    type="text"
                    placeholder="Localisation"
                    name="localisation"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.localisation}
                  />
                </div> */}
                <div className="col-lg-6 col-md-6">
                  <span>Prix (MAD) </span>
                  <input
                    type="text"
                    placeholder="Prix"
                    name="prix"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.prix}
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <span>Avance (MAD) </span>
                  <input
                    type="text"
                    placeholder="Avance"
                    name="avance"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.avance}
                  />
                </div>

                <div className="col-lg-6 col-md-6 ">
                  {/* <input type="text" placeholder="Eleveur" /> */}
                  <span>Sexe </span>
                  <Select
                    value={selectedOptionSexe}
                    onChange={this.handleChangeSexe}
                    options={optionsSexe}
                    placeholder="Sexe"
                    required
                    // className="Select"
                    // styles={customStyles}
                  />
                  <br />
                </div>
                <div className="col-lg-6 col-md-6">
                  {/* <input type="text" placeholder="Eleveur" /> */}
                  <span>Race </span>
                  <Select
                    value={selectedOptionRace}
                    onChange={this.handleChangeRace}
                    options={optionsRace}
                    placeholder="Race"
                    required
                    // className="Select"
                  />
                  <br />
                </div>

                <div className="col-lg-12 col-md-12">
                  {/* <input type="text" placeholder="Eleveur" /> */}
                  <span>Eleveur </span>
                  <Select
                    value={selectedOption}
                    onChange={this.handleChange}
                    options={options}
                  />
                  <br />
                </div>

                <div className="col-lg-6 col-md-6">
                  <span>Age (mois) </span>
                  <input
                    type="int"
                    placeholder="Age"
                    name="age"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.age}
                  />
                </div>
                <div className="col-lg-6 ">
                  <span>Image face</span>
                  <input
                    type="file"
                    placeholder="Images"
                    onChange={this.handleChangeImageFace}
                    required
                  />
                </div>
                <div className="col-lg-6 ">
                  <span>Image profile</span>
                  <input
                    type="file"
                    placeholder="Images"
                    onChange={this.handleChangeImageProfile}
                    required
                  />
                </div>
                <div className="col-lg-6 ">
                  <span>Image boucle</span>
                  <input
                    type="file"
                    placeholder="Images"
                    onChange={this.handleChangeImageBoucle}
                    required
                  />
                </div>
                <div className="col-lg-12 text-center">
                  <textarea
                    placeholder="Description "
                    name="description"
                    onChange={this.onChange}
                    defaultValue={this.state.mot.description}
                  ></textarea>
                  <button type="submit" className="site-btn">
                    Modifier
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default UpdateMouton;
