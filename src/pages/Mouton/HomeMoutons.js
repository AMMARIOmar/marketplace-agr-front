import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
class HomeMoutons extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      Annonces: [],
      redirect: false,
      url: "Images/1.jpg",
      ideleveur: "",
    };
  }

  componentDidMount() {
    if (!localStorage.getItem("usertoken")) {
      this.props.history.push("/login");
    } else {
      const idt = localStorage.getItem("usertoken");
      axios
        .get("http://127.0.0.1:8000/api/technicien/" + idt + "/moutons", {
          headers: {
            // "x-access-token": token, // the token is a variable which holds the token
          },
        })
        .then((res) => {
          console.log(res);
          this.setState({
            Annonces: res.data.moutons,
          });
        });
    }
  }
  handelDelete(clicked_id) {
    console.log(clicked_id);
    var cmd = [];
    axios
      .get("http://127.0.0.1:8000/api/commande", {
        headers: {
          // "x-access-token": token, // the token is a variable which holds the token
          "Content-Type": "application/json",
        },
        params: {
          id_mouton: clicked_id,
          order_mode: "asc",
          order_by: "date_creation",
        },
      })
      .then((res) => {
        // this.setState({
        //   Annonces: res.data,
        // });
        cmd = res.data;
        // console.log(cmd.length);
        if (cmd.length === 0) {
          if (window.confirm(`Voulez vous vraiment supprimer ce mouton ?`)) {
            axios
              .get("http://127.0.0.1:8000/api/mouton/" + clicked_id, {
                headers: {
                  // "x-access-token": token, // the token is a variable which holds the token
                  "Content-Type": "application/json",
                },
              })

              .then((res) => {
                console.log(res.data.objet.id_eleveur);
                console.log(this.state.ideleveur);
                this.setState({ ideleveur: res.data.objet.id_eleveur });
                console.log(this.state.ideleveur);
                axios
                  .delete("http://127.0.0.1:8000/api/mouton/" + clicked_id, {
                    headers: {
                      // "x-access-token": token, // the token is a variable which holds the token
                    },
                  })
                  .then((res) => {
                    //  console.log("Mouton: "+ clicked_id);
                    //  console.log("Eleveur:"+ clicked_id);
                    axios.put(
                      "http://127.0.0.1:8000/api/eleveur/" +
                        this.state.ideleveur +
                        "/mouton/" +
                        clicked_id,

                      {
                        headers: { "Content-Type": "application/json" },
                      }
                    ).then(()=>{window.location.reload();})
                    
                  });
              });
            

          } else {
          }
        } else {
          alert(
            "Ce mouton ne peut pas être supprimé pour le moment, car il est sur une commande"
          );
        }

        //-----------------
      });
    // cmd = res.data;
    // // console.log(cmd.length);
    // if (cmd.length === 0) {
    //   if (window.confirm(`Voulez vous vraiment supprimer ce mouton ?`)) {
    //     axios
    //       .delete("http://127.0.0.1:8000/api/mouton/" + clicked_id, {
    //         headers: {
    //           // "x-access-token": token, // the token is a variable which holds the token
    //         },
    //       })
    //       .then((res) => {
    //         //delete id eleveur from technicien
    //         this.props.history.push("/Annonces");
    //         this.componentDidMount();
    //       });
    //   } else {
    //   }
    // } else {
    //   alert(
    //     "Ce mouton ne peut pas être supprimé pour le moment, car il est sur une commande"
    //   );
    // }
  }
  render() {
    let Moutons = this.state.Annonces;
    return (
      <div>
        <center>
          <div className="col-lg-10 col-md-10">
            <div className="filter__item">
              <div className="row">
                <div className="col-lg-4 col-md-4">
                  <div className="filter__found text-left">
                    <h6>
                      <span>{Moutons.length}</span> Têtes de moutons au total
                    </h6>
                  </div>
                </div>
                <div className="col-lg-8 col-md-3 text-left">
                  <div className="filter__option">
                    <a href="/AddAnnonce">
                      {" "}
                      <button className="site-btn ">Ajouter mouton</button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              {this.state.Annonces.map((Annonces) => (
                <div className="col-lg-4 col-md-6 col-sm-6" key={Annonces._id}>
                  <div className="product__item">
                    <div
                      className="product__item__pic set-bg"
                      data-setbg={this.state.url}
                    >
                      <img
                        src={Annonces.image_face}
                        className="product__item__pic set-bg"
                      />
                      <ul className="product__item__pic__hover">
                        <li>
                          <Link
                            id={Annonces._id}
                            onClick={(e) =>
                              this.handelDelete(e.currentTarget.id)
                            }
                          >
                            <a href="#">
                              <i class="fa fa-trash"></i>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to={{
                              pathname: "/UpdateAnnonce",
                              state: {
                                id: Annonces._id,
                              },
                            }}
                          >
                            {" "}
                            <a href="#">
                              <i class="fa fa-wrench"></i>
                            </a>
                          </Link>
                        </li>
                        {/* <li>
                        <a href="#">
                          <i className="fa fa-shopping-cart"></i>
                        </a>
                      </li> */}
                        <li>
                          <Link
                            to={{
                              pathname: "/DetailsMouton",
                              state: {
                                id: Annonces._id,
                              },
                            }}

                            // to={`/DetailsMouton/${Annonces._id}`}
                          >
                            {" "}
                            <a href="#">
                              <i class="fa fa-eye"></i>
                            </a>
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="product__item__text">
                      <h6>{"         " + Annonces.race}</h6>
                      <h6>{"         " + Annonces.poids + " Kg"}</h6>
                      <h6>{"         " + Annonces.age + " mois"}</h6>
                      <h5>{"         " + Annonces.prix + " MAD"}</h5>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            {/* <div className="product__pagination">
              <a href="#">1</a>
              <a href="#">2</a>
              <a href="#">3</a>
              <a href="#">
                <i className="fa fa-long-arrow-right"></i>
              </a>
            </div> */}
          </div>
        </center>
      </div>
    );
  }
}

export default HomeMoutons;
