import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
class Header extends Component {
  constructor() {
    super();
    // let redirect = false;
    this.state = {
      isLoged: false,
    };
    // this.HandelLogout = this.HandelLogout.bind(this);
  }
// var t=this;
  componentDidMount() {
    const token = localStorage.getItem("usertoken");
    if (token) {
      this.setState({ isLoged: true });
      // this.props.history.push("/login");
    }
  }

//   HandelLogout() {
//     localStorage.removeItem("usertoken");
//  this.props.history.push("login");
//     window.location.reload();
    
//   }
  render() {
    return (
      <div>
        <header className="header">
          <div className="header__top">
            <div className="container">
              <div className="row">
                <div className="col-lg-6 col-md-6">
                  <div className="header__top__left">
                    {/* <ul>
                      <li>
                        <i className="fa fa-envelope"></i> hello@colorlib.com
                      </li>
                      <li>Free Shipping for all Order of $99</li>
                    </ul> */}
                  </div>
                </div>
                <div className="col-lg-6 col-md-6">
                  <div className="header__top__right">
                    <div className="header__top__right__social">
                      <a href="#">
                        <i className="fa fa-facebook"></i>
                      </a>
                      <a href="#">
                        <i className="fa fa-twitter"></i>
                      </a>
                      <a href="#">
                        <i className="fa fa-youtube"></i>
                      </a>
                      {/* <a href="#">
                        <i className="fa fa-pinterest-p"></i>
                      </a> */}
                    </div>
                    <div className="header__top__right__language">
                      <img src="assets/img/language.png" alt="" />
                      <div>Français</div>
                      <span className="arrow_carrot-down"></span>
                      <ul>
                        <li>
                          <a href="#">Français</a>
                        </li>
                        {/* <li>
                          <a href="#">English</a>
                        </li> */}
                        <li>
                          <a href="#">العربية</a>
                        </li>
                      </ul>
                    </div>
                    <div className="header__top__right__auth">
                      {this.state.isLoged ? (
                        <div>
                          {" "}
                          <a href="/login" >
                            <i className="fa fa-user"> Se déconnecter</i>
                          </a>
                        </div>
                      ) : null}
                      {!this.state.isLoged ? (
                        <div>
                          {" "}
                          <a href="/login">
                            <i className="fa fa-user"> Se connecter</i>
                          </a>
                        </div>
                      ) : null}
                    </div>{" "}
                    <div className="header__top__right__auth">
                      {/* <a href="#">
                        <i className="fa fa-user"></i>
                      </a> */}
                      {/* <a href="#">
                        <i className="fa fa-envelope"></i> <span>3</span>
                      </a> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-lg-2">
                <div className="header__logo">
                  <a href="./index.html">
                    <img src="assets/img/logo.png" alt="" />
                  </a>
                </div>
              </div>
              <div className="col-lg-8">
                <nav className="header__menu">
                  <ul>
                    <li className="active">
                      <a href="./Eleveurs"> éleveurs</a>
                    </li>
                    <li>
                      <a href="./Annonces"> moutons</a>
                    </li>
                    <li>
                      <a href="./HomeCommandeValidation"> commandes</a>
                    </li>

                    <li>
                      <a href="/AddAnnonce"> Ajouter une annonce</a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="col-lg-2">
                <div className="header__cart">
                  <ul>
                    <li>
                      {/* <a href="#">
                        <i className="fa fa-user"></i> 
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fa fa-envelope"></i> <span>3</span>
                      </a> */}
                    </li>
                  </ul>
                  {/* <div className="header__cart__price">
                    item: <span>$150.00</span>
                  </div> */}
                </div>
              </div>
            </div>
            <div className="humberger__open">
              <i className="fa fa-bars"></i>
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default Header;
