import React from "react";
// import logo from "./logo.svg";
import "./App.css";
// import Home from "./pages/Home";
// import Test from "./pages/Test";
// import Header from "./pages/Header";
// import Footer from "./pages/Footer";
// import AddMouton from "./pages/AddMouton";
// import HomeCommande from "./pages/HomeCommande";
// import DetailsCommande from "./pages/DetailsCommande";
// import HomeEleveur from "./pages/HomeEleveur";
// import AddEleveur from "./pages/AddEleveur";
// import DetailsEleveur from "./pages/DetailsEleveur";
// import SignUp from "./pages/SignUp";
import Container from "./containers/Container"
import ContainerAdmin from "./containers/ContainerAdmin"
import { BrowserRouter, Route } from "react-router-dom";
function App() {
  return (
    <div>
      {/* <Header /> */}
      {/* <Test />*/}
      {/* <Home />  */}
      {/* <AddMouton/> */}
      {/* <HomeCommande /> */}
      {/* <DetailsCommande /> */}
      {/* <HomeEleveur /> */}
      {/* <AddEleveur /> */}
      {/* <DetailsEleveur /> */}
      {/* <Container /> */}
      {/* <SignUp /> */}
      {/* <Footer /> */}
      <BrowserRouter>
          <Route exact path={["/Annonces","/Eleveurs","/AddAnnonce","/DetailsCommande","/ValidationDetailsCommande","/AddEleveur","/DetailsEleveur","/UpdateEleveur","/UpdateAnnonce","/login","/","/register","/DetailsMouton","/HomeCommandePrisEnCharge","/HomeCommandesHistorique","/HomeCommandeValidation","/MoutonsEleveur"]} component={Container} />
          <Route exact path="/Techniciens" component={ContainerAdmin} />

          {/* DetailsCommande */}
        </BrowserRouter>
    </div>
  );
}

export default App;
